set :stage, :production

set :deploy_to, '~/apps/aurika'

set :branch, 'master'

set :rails_env, 'production'

# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary
# server in each group is considered to be the first
# unless any hosts have the primary property set.
role :app, %w{carpfishing@77.221.144.208}
role :web, %w{carpfishing@77.221.144.208}
role :db,  %w{carpfishing@77.221.144.208}