# coding: utf-8
AuricaCurkan::Application.routes.draw do
  root :to => 'main#index'

  devise_for :users

    mount RailsAdmin::Engine => '/admin', :as => 'rails_admin'

    resources :contacts
    resources :events

    # delete '/logout' => 'sessions#destroy', :as => :logout
    # get '/login' => 'sessions#new', :as => :login
    # post '/register' => 'users#create', :as => :register
    # get '/signup' => 'users#new', :as => :signup
    #resources :users
    # resource :session
    get '/foto' => 'fotos#index', :as => :foto_index
    get '/video' => 'videos#index', :as => :video_index
    get '/about' => 'main#about', :as => :about_index
    get '/schedule' => 'main#schedule', :as => :schedule
    get '/payment' => 'main#payment', :as => :payment
    get '/camp_scheld' => 'main#camp_scheld', :as => :camp_scheld
    get '/help' => 'main#help', :as => :help
    get '/dance' => 'main#about_dance', :as => :about_dance_index
    get '/payment_order' => 'main#payment_order', :as => :payment_order
    resources :videos
    resources :publications
    resources :foto_albums
    resources :collectives
    resources :fotos do
      collection do
        post :swfupload
      end


    end

    resources :foto_albums
    resources :news
  # match '/:controller(/:action(/:id))'
end
